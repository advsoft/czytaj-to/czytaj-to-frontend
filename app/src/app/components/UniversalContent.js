/**
 * Created by johniak on 1/7/17.
 */
import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import reactStringReplace from 'react-string-replace-recursively';
import {Button} from 'react-toolbox/lib/button';
import FontIcon from 'react-toolbox/lib/font_icon';
import ImageGallery from './ImageGallery/ImageGallery';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  line: {
    width: '100%',
    height: 1,
    marginBottom: 1,
    marginTop: 1,
    backgroundColor: '#373737',
    flexGrow: 1
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 3,
    alignItems: 'center'
  },
  content: {
    flexDirection: 'row',
    whiteSpace: 'pre-line'
  },
  link: {
    color: '#6cb0dd',
    textDecoration: 'none'
  },
  authorLink: {
    color: '#ee2222',
    textDecoration: 'none',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  avatar: {
    width: 50,
    height: 50,
    margin: 5,
    marginLeft: 0
  },
  likesContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  likeButton: {
    width: 30,
    height: 30,
    minWidth: 30,
    marginLeft: 10,
    marginRight: 10,
    lineHeight: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  likeButtonFont: {
    fontSize: 15,
    lineHeight: 1
  },
  answer: {
    minWidth: 0,
    width: 130,
    height: 20,
    fontSize: 12,
    lineHeight: 1,
    margin: 5
  },
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%'
  },
  videoContainer: {
    position: 'relative',
    paddingBottom: '56.25%',
    height: 0,
    overflow: 'hidden',
    marginBottom: 10,
    marginTop: 10
  },
  article: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff'
  },
  articleContainer: {
    width: '100%',
    height: 600
  },
  articleContainerFullescreen: {
    left: 0,
    top: 40,
    position: 'fixed',
    width: '100vw',
    height: 'calc(100vh - 40px)',
    zIndex: 8000
  },
  articleParent: {
    width: '100%',
    height: '100%',
    position: 'relative'
  },
  fullscreenButton: {
    position: 'absolute',
    right: 15,
    top: 15,
    alignText: 'center',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)'
  },
  openButton: {
    position: 'absolute',
    right: 15,
    bottom: 15,
    alignText: 'center',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)'
  },
  articleOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: '#77222222'
  },
  inactiveArticle: {
    filter: 'blur(5px) grayscale(80%)'
  },
  activatePanel: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    cursor: 'pointer'
  },
  activateIcon: {
    fontSize: 60,
    padding: 5,
    backgroundColor: 'rgba(100, 100, 100, 0.5)',
    borderRadius: 5
  },

  fullscreenIcon: {
    fontSize: 30
  }
};

class UniversalContent extends Component {
  constructor() {
    super();
    this.handleAnswerClick = this.handleAnswerClick.bind(this);
    this.handleFullscreen = this.handleFullscreen.bind(this);
    this.handleLike = this.handleLike.bind(this);
    this.handleActivateArticle = this.handleActivateArticle.bind(this);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {articleFullscreen: false, articleActive: false};
  }

  handleAnswerClick() {
    console.log('UniversalContent', 'handleAnswerClick');
    this.props.onAnswer(this.props.content);
  }

  handleFullscreen() {
    this.setState({articleFullscreen: !this.state.articleFullscreen});
    console.log(this.state);
  }

  handleLike() {
    const {content, type} = this.props;
    console.log(content);
    this.props.actions.toggleLike(type, content.id, content.is_liked, content);
  }

  handleActivateArticle() {
    this.setState({articleActive: true});
  }

  renderContent() {
    const config = {
      hashTag: {
        pattern: /#(\w\w+)/ig,
        matcherFn: (rawText, processed, key) => {
          return (
            <span
              key={key}
            >#
              <Link
                style={styles.link}
                to={`/tag/${rawText}/`}
              >
                {rawText}
              </Link>
            </span>);
        }
      },
      user: {
        pattern: /@(\w\w+)/ig,
        matcherFn: (rawText, processed, key) => {
          return (
            <span
              key={key}
            >@
              <Link
                style={styles.link}
                to={`/user/${rawText}/`}
              >
                {rawText}
              </Link>
            </span>);
        }
      }
    };

    const {content} = this.props.content;
    const result = reactStringReplace(config)(content);
    return result;
  }

  renderImages() {
    const {content} = this.props;
    if (!('media_set' in content) || content.media_set.length < 1) {
      return null;
    }
    const images = [];
    for (const media of content.media_set) {
      images.push({original: media.image, thumbnail: media.image});
    }
    return (
      <ImageGallery
        items={images}
        slideInterval={2000}
        onImageLoad={this.handleImageLoad}
      />
    );
  }

  renderArticle() {
    const articleUrl = this.props.content.article_url;
    if (!articleUrl) {
      return null;
    }
    if (articleUrl.startsWith('https://www.youtube.com/watch?v=') || articleUrl.startsWith('http://www.youtube.com/watch?v=')) {
      return this.renderYoutube(articleUrl);
    }
    if (articleUrl.startsWith('https://www.liveleak.com/view') || articleUrl.startsWith('http://www.liveleak.com/view')) {
      return this.renderLiveLeak(articleUrl);
    }
    if (articleUrl.startsWith('https://vimeo.com/') || articleUrl.startsWith('http://vimeo.com/')) {
      return this.renderVimeo(articleUrl);
    }
    return this.renderOtherArticle(articleUrl);
  }

  renderOtherArticle(url) {
    const articleImage = this.props.content.article_image;
    console.log('articleImage', articleImage, this.state.articleActive && articleImage);
    return (
      <div style={this.state.articleFullscreen ? styles.articleContainerFullescreen : styles.articleContainer}>
        <div style={styles.articleParent}>
          <Button
            style={styles.fullscreenButton} onClick={this.handleFullscreen}
            raised
            accent
          >
            {this.state.articleFullscreen ?
              <FontIcon style={styles.fullscreenIcon} value="fullscreen_exit"/>
              :
              <FontIcon style={styles.fullscreenIcon} value="fullscreen"/>
            }
          </Button>
          <Button
            style={styles.openButton}
            target="_blank"
            href={url}
            raised
            accent
          >
            <FontIcon style={styles.fullscreenIcon} value="tab"/>
          </Button>
          {
            this.state.articleActive || !articleImage ?
              <iframe
                sandbox="allow-same-origin allow-scripts allow-popups allow-forms"
                frameBorder="0"
                src={`${url}`}
                style={{...styles.article, ...(this.state.articleActive ? {} : styles.inactiveArticle)}}
              />
              :
              <img style={{...(this.state.articleActive ? {} : styles.inactiveArticle)}} src={articleImage}/>
          }
          <div
            style={{...styles.articleOverlay, ...(this.state.articleActive ? {display: 'none'} : {})}}
            onClick={this.handleActivateArticle}
          >
            <div style={styles.activatePanel}>
              <FontIcon style={styles.activateIcon} value="slideshow"/>
            </div>
          </div>
        </div>
      </div>);
  }

  renderYoutube(youtubeUrl) {
    let id = youtubeUrl.split('youtube.com/watch?v=')[1];
    id = id.replace('&', '?');
    const embedUrl = `https://www.youtube.com/embed/${id}/`;
    return (
      <div style={styles.videoContainer}>
        <iframe
          style={styles.video}
          src={embedUrl}
          frameBorder="0"
          width="720"
          height="405"
          allowFullScreen
        />
      </div>
    );
  }

  renderLiveLeak(youtubeUrl) {
    const id = youtubeUrl.split('liveleak.com/view')[1];
    const embedUrl = `https://www.liveleak.com/ll_embed${id}`;
    return (
      <div style={styles.videoContainer}>
        <iframe
          style={styles.video}
          src={embedUrl}
          frameBorder="0"
          width="720"
          height="405"
          allowFullScreen
        />
      </div>
    );
  }

  renderVimeo(youtubeUrl) {
    const id = youtubeUrl.split('vimeo.com/')[1];
    const embedUrl = `https://player.vimeo.com/video/${id}`;
    return (
      <div style={styles.videoContainer}>
        <iframe
          style={styles.video}
          src={embedUrl}
          frameBorder="0"
          width="720"
          height="405"
          allowFullScreen
        />
      </div>
    );
  }

  render() {
    const {content, auth} = this.props;
    return (<div style={styles.container}>
      <div style={styles.header}>
        <a style={styles.authorLink} href={`/user/${content.author.username}`}>
          <img
            style={styles.avatar}
            src={content.author.avatar ? content.author.avatar : 'https://assets.materialup.com/uploads/e8954b98-a041-457c-a2b8-a1cc6937bf26/avatar.png'}
          />
          {content.author.username}
        </a>
        <div style={styles.likesContainer}>{content.likes_count}
          {auth.isAuthenticated ?
            <Button
              style={styles.likeButton}
              accent
              mini
              raised={content.is_liked}
              onClick={this.handleLike}
            >
              <FontIcon style={styles.likeButtonFont} value="favorite"/>
            </Button> : null
          }
        </div>
      </div>
      <div style={styles.line}/>
      <div style={styles.content}>{this.renderContent()}</div>
      {this.renderArticle()}
      <div>{this.renderImages()}</div>
      {auth.isAuthenticated ?
        <Button onClick={this.handleAnswerClick} style={styles.answer} primary>Odpowiedz</Button> : null
      }
    </div>);
  }
}

UniversalContent.propTypes = {
  content: PropTypes.object.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  onAnswer: PropTypes.func.isRequired,
  auth: PropTypes.object,
  actions: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired
};

export default UniversalContent;
