/**
 * Created by johniak on 1/7/17.
 */
import React, {Component, PropTypes} from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import Post from './Post';
import moment from 'moment';
import _ from 'lodash';

const styles = {
  container: {}
};

class PostsList extends Component {
  constructor() {
    super();
    this.loadMore = this.loadMore.bind(this);
  }

  componentWillMount() {
    this.getPosts(this.props);
  }

  componentWillReceiveProps(nextProps) {
    console.log();
    if (!_.isEqual(nextProps.routeParams, this.props.routeParams)) {
      this.getPosts(nextProps);
    }
  }

  getPosts(props) {
    const params = {};
    if (props.routeParams.tag) {
      params.tags = [props.routeParams.tag];
    }
    if (props.routeParams.user) {
      params.author = props.routeParams.user;
    }
    let dateFrom;
    switch (props.routeParams.type) {
      case 'hot6':
        params.orderBy = '-likes,-date';
        dateFrom = moment();
        dateFrom.add(-6, 'h');
        params.dateFrom = dateFrom.utc().format();
        params.dateTo = moment().utc().format();
        break;
      case 'hot12':
        params.orderBy = '-likes,-date';
        dateFrom = moment();
        dateFrom.add(-12, 'h');
        params.dateFrom = dateFrom.utc().format();
        params.dateTo = moment().utc().format();
        break;
      case 'hot24':
        params.orderBy = '-likes,-date';
        dateFrom = moment();
        dateFrom.add(-24, 'h');
        params.dateFrom = dateFrom.utc().format();
        params.dateTo = moment().utc().format();
        break;
      default:
    }
    this.props.actions.getPosts(0, 6, params);
  }

  renderPosts() {
    const posts = [];
    for (const post of this.props.microblog.posts) {
      posts.push(<Post {...this.props} key={post.id} post={post}/>);
    }
    return posts;
  }

  loadMore() {
    const {offset, limit} = this.props.microblog.params;
    this.props.actions.getPosts(offset + 1, limit, this.props.microblog.params);
  }

  render() {
    const {offset, limit, count} = this.props.microblog.params;
    let loadMore = false;
    if (offset !== undefined && count !== undefined && limit !== undefined) {
      loadMore = (offset + limit) < count;
    }
    return (
      <div style={styles.container}>
        <InfiniteScroll
          pageStart={0}
          loadMore={this.loadMore}
          hasMore={loadMore}
          loader={<div className="loader">Loading ...</div>}
        >
          {this.renderPosts()}
        </InfiniteScroll>
      </div>);
  }
}
PostsList.propTypes = {
  microblog: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  routeParams: PropTypes.object,
  actions: PropTypes.fun
};

export default PostsList;
