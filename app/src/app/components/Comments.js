/**
 * Created by johniak on 1/13/17.
 */

import React, {Component, PropTypes} from 'react';
import Comment from './Comment';
import Answer from './Answer';

const styles = {
  comments: {
    marginTop: 15
  },
  childComments: {
    marginLeft: 50
  }
};

export default class Comments extends Component {
  constructor() {
    super();
    this.state = {selectedComment: null};
    this.handleAnswerClick = this.handleAnswerClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleAnswerClick(comment) {
    console.log('Comments', 'handleAnswerClick');
    this.setState({selectedComment: comment});
    console.log(comment.id);
  }

  isChildComment(comment, selectedComment) {
    if (comment.comments && selectedComment) {
      return comment.comments.includes(selectedComment);
    }
    return false;
  }

  handleSubmit() {
    this.setState({selectedComment: null});
  }

  render() {
    const {comments} = this.props;
    const commentsList = [];
    for (const comment of comments) {
      commentsList.push(
        <Comment
          onAnswer={this.handleAnswerClick}
          {...this.props}
          key={comment.id}
          comment={comment}
        />);
      if (comment.comments && comment.comments.length > 0) {
        commentsList.push(
          <div style={styles.childComments} key={`childs${comment.id}`}>
            <Comments
              {...this.props}
              onAnswer={this.handleAnswerClick}
              comments={comment.comments}
            />
          </div>);
      }
      if (comment === this.state.selectedComment || this.isChildComment(comment, this.state.selectedComment)) {
        commentsList.push(
          <Answer
            key={`answer-${comment.id}`}
            onSubmit={this.handleSubmit}
            {...this.props}
            type="comment"
            parent={comment}
            recipient={this.state.selectedComment.author}
          />
        );
      }
    }
    if (commentsList.length < 1) {
      return null;
    }
    return (<div style={styles.comments}>{commentsList}</div>);
  }
}

Comments.propTypes = {
  post: PropTypes.object.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  comments: PropTypes.array.isRequired,
  onAnswer: PropTypes.func
};
