/**
 * Created by johniak on 1/13/17.
 */

import React, {Component, PropTypes} from 'react';
import Input from 'react-toolbox/lib/input';
import Button from 'react-toolbox/lib/button';
import theme from '../theme/input.scss';

const styles = {
  container: {
    backgroundColor: '#555555',
    padding: 10,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },

  addButton: {
    minWidth: 0,
    width: 130,
    height: 30,
    fontSize: 15,
    lineHeight: 1,
    marginTop: 5
  }
};

export default class Answer extends Component {
  constructor() {
    super();
    this.handleChange();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {answer: ''};
  }

  handleChange(answer) {
    this.setState({answer});
  }

  handleSubmit() {
    const {actions, parent, type} = this.props;
    const {answer} = this.state;
    actions.createComment(type, parent, answer);
    const {onSubmit} = this.props;
    if (onSubmit) {
      onSubmit(type, parent, answer);
    }
    this.clear();
  }

  componentWillMount() {
    if (this.props.recipient) {
      this.setState({answer: `${this.state.answer}@${this.props.recipient.username}: `});
    }
  }

  clear() {
    if (this.props.recipient) {
      this.setState({answer: `@${this.props.recipient.username}: `});
      return;
    }
    this.setState({answer: ''});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.recipient && nextProps.recipient.username !== this.props.recipient.username) {
      this.setState({answer: `${this.state.answer}@${nextProps.recipient.username}: `});
    }
  }

  render() {
    return (
      <div style={styles.container}>
        <form onSubmit={this.handleSubmit}>
          <Input
            type="text"
            multiline
            label="Komentarz"
            value={this.state.answer}
            onChange={this.handleChange}
            theme={theme}
          />
          <Button
            onClick={this.handleSubmit}
            style={styles.addButton}
            icon="add"
            label="Dodaj"
            primary
            raised
          />
        </form>
      </div>);
  }
}

Answer.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  parent: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  recipient: PropTypes.object,
  actions: PropTypes.object.isRequired,
  onSubmit: PropTypes.func
};
