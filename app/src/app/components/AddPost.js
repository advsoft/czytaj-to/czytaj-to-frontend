/**
 * Created by johniak on 1/13/17.
 */

import React, {Component, PropTypes} from 'react';
import Input from 'react-toolbox/lib/input';
import Button from 'react-toolbox/lib/button';
import theme from '../theme/input.scss';
import ImageInput from './ImageInput';

const styles = {
  container: {
    backgroundColor: '#555555',
    padding: 10,
    borderRadius: 3
  },

  addButton: {
    minWidth: 0,
    width: 130,
    height: 30,
    fontSize: 15,
    lineHeight: 1,
    marginTop: 5,
    alignSelf: 'right'
  },
  addPhotoButton: {
    minWidth: 0,
    height: 30,
    fontSize: 15,
    lineHeight: 1,
    marginTop: 5,
    alignSelf: 'right'
  },
  addArticleUrlButton: {
    minWidth: 0,
    height: 30,
    fontSize: 15,
    lineHeight: 1,
    marginTop: 5,
    alignSelf: 'right'
  }
};

export default class AddPost extends Component {
  constructor() {
    super();
    this.handleChange();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAddPhoto = this.handleAddPhoto.bind(this);
    this.handlePhotoChange = this.handlePhotoChange.bind(this);
    this.handleAddArticle = this.handleAddArticle.bind(this);
    this.state = {content: '', inputs: [], medias: {}, isArticleVisible: false, articleUrl: undefined};
  }

  handleChange(content, event) {
    if (!event) {
      return;
    }
    const newState = {};
    newState[event.nativeEvent.target.name] = content;
    this.setState(newState);
  }

  handleSubmit() {
    const {actions} = this.props;
    const {content} = this.state;
    const {articleUrl} = this.state;
    const {onSubmit} = this.props;
    const medias = [];
    for (const key in this.state.medias) {
      if (!Object.prototype.hasOwnProperty.call(this.state.medias, key)) {
        break;
      }
      medias.push({image: this.state.medias[key]});
    }
    actions.createPost(content, medias, articleUrl);
    if (onSubmit) {
      onSubmit({content, medias});
    }
    this.clear();
  }

  clear() {
    this.setState({content: '', inputs: [], medias: {}, isArticleVisible: false, articleUrl: undefined});
  }

  handlePhotoChange(data, target) {
    const {medias} = this.state;
    medias[target.props.name] = data;
    console.log('key', medias);
    this.setState({medias});
  }

  handleAddPhoto() {
    if (Object.keys(this.state.medias).length < this.state.inputs.length) {
      return;
    }
    this.state.inputs.push((
      <ImageInput onChange={this.handlePhotoChange} name={`media_${this.state.inputs.length}`}/>));
    this.setState({inputs: this.state.inputs});
  }

  handleAddArticle() {
    this.setState({isArticleVisible: true});
  }

  render() {
    return (
      <div style={styles.container}>
        <form onSubmit={this.handleSubmit}>
          <Input
            type="text"
            name="content"
            multiline
            label="Post"
            value={this.state.content}
            onChange={this.handleChange}
            theme={theme}
          />
          <div>
            {this.state.inputs}
            <div>
              {this.state.isArticleVisible ?
                <Input
                  type="text"
                  name="articleUrl"
                  label="Article url"
                  value={this.state.articleUrl}
                  onChange={this.handleChange}
                  theme={theme}
                />
                :
                null
              }
            </div>
            {Object.keys(this.state.medias).length >= this.state.inputs.length ?
              <Button
                onClick={this.handleAddPhoto}
                style={styles.addPhotoButton}
                label="Dodaj zdjecie"
                accent
              />
              :
              null
            }
            {this.state.isArticleVisible ?
              null
              :
              <Button
                onClick={this.handleAddArticle}
                style={styles.addPhotoButton}
                label="Dodaj artykul"
                accent
              />
            }
          </div>
          <Button
            onClick={this.handleSubmit}
            style={styles.addButton}
            label="Dodaj"
            primary
            raised
          />
        </form>
      </div>);
  }
}

AddPost.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
  onSubmit: PropTypes.func
};
