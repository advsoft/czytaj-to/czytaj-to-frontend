/**
 * Created by johniak on 1/13/17.
 */

import React, {Component, PropTypes} from 'react';

const styles = {
  container: {
    padding: 5,
    display: 'flex',
    alignItems: 'center'
  },

  addButton: {
    minWidth: 0,
    width: 130,
    height: 30,
    fontSize: 15,
    lineHeight: 1,
    marginTop: 5
  },
  previewBox: {
    display: 'flex',
    width: 100,
    height: 100,
    borderStyle: 'dashed',
    borderColor: 'white',
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  preview: {
    display: 'flex',
    maxHeight: 100,
    maxWidth: 100
  }
};

export default class ImageInput extends Component {
  constructor() {
    super();
    this.state = {image: null, preview: 'http://placehold.it/350x150'};
    this.handleFileChange = this.handleFileChange.bind(this);
  }

  handleFileChange(event) {
    const element = event.nativeEvent.target;
    if (element.files.length < 1) {
      return;
    }
    const file = element.files[0];
    const reader = new FileReader();
    reader.onloadend = e => {
      const {result} = e.target;
      this.setState({image: result, preview: result});
      this.props.onChange(result, this);
    };
    reader.readAsDataURL(file);
  }

  render() {
    return (
      <div style={styles.container}>
        <div style={styles.previewBox}>
          <img style={styles.preview} src={this.state.preview}/>
        </div>
        <input onChange={this.handleFileChange} type="file"/>
      </div>);
  }
}

ImageInput.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired
  // parent: PropTypes.object.isRequired,
  // type: PropTypes.string.isRequired,
  // recipient: PropTypes.object,
  // actions: PropTypes.object.isRequired,
  // onSubmit: PropTypes.func
};
