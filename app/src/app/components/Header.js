/**
 * Created by johniak on 1/7/17.
 */
import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import Radium from 'radium';
import FontIcon from 'react-toolbox/lib/font_icon';

const styles = {
  container: {
    background: '#3c3c3c',
    color: '#fff',
    display: 'flex',
    flexGrow: '1',
    '@media (min-width: 600px)': {
      justifyContent: 'space-between'
    },
    '@media (max-width: 599px)': {
      flexDirection: 'column'
    },
    position: 'fixed',
    width: '100%',
    zIndex: 9000,
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)'
  },

  link: {
    color: '#fff',
    textDecoration: 'none'
  },
  options: {
    display: 'flex',
    '@media (min-width: 600px)': {
      flexDirection: 'row'
    },
    '@media (max-width: 599px)': {
      flexDirection: 'column',
      alignItems: 'stretch',
      display: 'none'
    },
    justifyContent: 'center',
    textAlign: 'center',
    transition: ''
  },
  option: {
    padding: 10
  },
  selectedOption: {
    borderBottom: '2px solid #f39c12',
    background: '#2c2c2c'
  },
  icon: {
    color: '#f39c12',
    fontSize: 'inherit'
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    '@media (max-width: 599px)': {
      display: 'flex',
      borderBottom: '1px solid #6c6c6c'
    }
  },
  hamburger: {
    '@media (min-width: 600px)': {
      display: 'none'
    },
    '@media (max-width: 599px)': {
      display: 'flex'
    },
    padding: 10
  }
};
@Radium
class Header extends Component {
  constructor() {
    super();
    this.handleHamburgerClick = this.handleHamburgerClick.bind(this);
    this.handleSingOut = this.handleSingOut.bind(this);
    this.state = {menuVisible: false};
  }

  handleHamburgerClick() {
    if (this.state.menuVisible) {
      styles.options['@media (max-width: 599px)'].display = 'none';
    } else {
      styles.options['@media (max-width: 599px)'].display = 'flex';
    }
    this.setState({menuVisible: !this.state.menuVisible});
  }

  handleSingOut() {
    console.log('test');
    this.props.actions.signOut();
  }

  render() {
    const {auth} = this.props;
    const {type} = this.props.routeParams;
    const {tag, user} = this.props.routeParams;
    let path = this.props.route.path.replace('(', '').replace(')', '');
    path = path.replace(':tag', this.props.routeParams.tag);
    path = path.replace(':user', this.props.routeParams.user);
    return (
      <div style={styles.container}>
        <div style={styles.header}>
          <div style={styles.option}><Link style={styles.link} to={{pathname: '/'}}>czytaj.to</Link></div>
          <div> {tag ? `#${tag}` : null} {user ? `@${user}` : null}</div>
          <div style={styles.hamburger} onClick={this.handleHamburgerClick}>&#9776;</div>
        </div>
        <div style={styles.options}>
          <Link style={styles.link} to={{pathname: path.replace(':type', 'hot6')}}>
            <div style={type === 'hot6' ? {...styles.option, ...styles.selectedOption} : styles.option}>
              <FontIcon style={styles.icon} value="whatshot"/> 6h
            </div>
          </Link>
          <Link style={styles.link} to={{pathname: path.replace(':type', 'hot12')}}>
            <div style={type === 'hot12' ? {...styles.option, ...styles.selectedOption} : styles.option}>
              <FontIcon style={styles.icon} value="whatshot"/> 12h
            </div>
          </Link>
          <Link style={styles.link} to={{pathname: path.replace(':type', 'hot24')}}>
            <div style={type === 'hot24' ? {...styles.option, ...styles.selectedOption} : styles.option}>
              <FontIcon style={styles.icon} value="whatshot"/> 24h
            </div>
          </Link>
          <Link style={styles.link} to={{pathname: path.replace(':type', 'newest')}}>
            <div
              style={type === undefined || type === 'newest' ? {...styles.option, ...styles.selectedOption} : styles.option}
            >
              Najnowsze
            </div>
          </Link>
        </div>
        <div
          style={{
            ...styles.options, ...{width: 90}, ...(this.state.menuVisible ? {display: 'flex !important', width: '100%'} : {})
          }}
        >
          {auth.isAuthenticated ?
            <Link style={styles.link} onClick={this.handleSingOut}>
              <div style={styles.option}>
                Sign Out
              </div>
            </Link>
            :
            <Link style={styles.link} to={{pathname: '/signin/'}}>
              <div style={styles.option}>
                Sign In
              </div>
            </Link>
          }
        </div>
      </div>);
  }
}
Header.propTypes = {
  routeParams: PropTypes.object,
  route: PropTypes.object,
  auth: PropTypes.object,
  actions: PropTypes.func
};
/* eslint-disable */
export default Radium(Header);
