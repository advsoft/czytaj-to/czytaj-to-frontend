/**
 * Created by johniak on 1/7/17.
 */
import React, {Component, PropTypes} from 'react';
import Input from 'react-toolbox/lib/input';
import Button from 'react-toolbox/lib/button';
import theme from '../theme/input.scss';

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    flexGrow: 1
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
    borderRadius: 3,
    width: 450,
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: '#2c2c2c'
  },
  errorBox: {
    border: '1px solid #ebcccc',
    borderRadius: 3,
    backgroundColor: '#f2dede',
    color: '#a94442',
    padding: '.75rem 1.25rem'
  },
  submit: {
    marginTop: 10
  }
};

class SignIn extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: ''
    };
    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  handleSignIn(e) {
    e.preventDefault();
    const {signIn} = this.props;
    signIn(this.state.username, this.state.password);
  }

  handleChangeUsername(value) {
    this.setState({username: value});
  }

  handleChangePassword(value) {
    this.setState({password: value});
  }

  renderError() {
    if (!this.props.auth.error) {
      return null;
    }
    return (
      <div style={styles.errorBox}>
        {this.props.auth.error.error_description}
      </div>
    );
  }

  render() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/');
    }
    return (
      <div style={styles.container}>
        <form style={styles.form} onSubmit={this.handleSignIn}>
          {this.renderError()}
          <Input
            type="username"
            name="username"
            label="Username"
            value={this.state.username}
            onChange={this.handleChangeUsername}
            theme={theme}
            error={this.props.auth.error !== undefined}
          />
          <Input
            type="password"
            name="password"
            label="Password"
            value={this.state.password}
            onChange={this.handleChangePassword}
            theme={theme}
            error={this.props.auth.error !== undefined}
          />
          <Button
            type="submit"
            style={styles.submit}
            primary
            raised
          >Sign In</Button>
        </form>
      </div>);
  }
}
SignIn.propTypes = {
  signIn: PropTypes.func.isRequired,
  auth: PropTypes.object,
  history: PropTypes.object
};

export default SignIn;
