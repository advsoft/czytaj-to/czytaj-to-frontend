/**
 * Created by johniak on 1/7/17.
 */
import React, {Component, PropTypes} from 'react';
import Comments from './Comments';
import UniversalContent from './UniversalContent';
import Answer from './Answer';
import {Button} from 'react-toolbox/lib/button';

const styles = {
  container: {
    background: '#2c2c2c',
    borderRadius: 3,
    color: '#fff',
    // padding: 10,
    display: 'flex',
    flexDirection: 'column',
    marginTop: 10
  },
  content: {
    padding: 10
  },
  showMore: {
    minWidth: 0,
    width: 130,
    height: 20,
    fontSize: 12,
    lineHeight: 1,
    margin: 5,
    display: 'flex',
    alignSelf: 'center'
  }
};

class Post extends Component {
  constructor() {
    super();
    this.handleShowMoreClick = this.handleShowMoreClick.bind(this);
    this.handleAnswerClick = this.handleAnswerClick.bind(this);
    this.state = {showAnswer: false};
  }

  handleShowMoreClick() {
    this.props.actions.getFullPost(this.props.post);
  }

  handleAnswerClick() {
    this.setState({showAnswer: true});
  }

  render() {
    const {post} = this.props;
    return (<div style={styles.container}>
      <div style={styles.content}>
        <UniversalContent {...this.props} content={post} onAnswer={this.handleAnswerClick} type="post"/>
      </div>
      <Comments {...this.props} comments={post.comments}/>
      {
        post.isFull || post.comments_count < 2 ?
          null :
          <Button onClick={this.handleShowMoreClick} style={styles.showMore} accent>Pokaż więcej</Button>
      }
      {
        this.state.showAnswer ?
          <Answer {...this.props} type="post" parent={post} recipient={post.author}/> :
          null
      }
    </div>);
  }
}
Post.propTypes = {
  post: PropTypes.object.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  actions: PropTypes.fun
};

export default Post;
