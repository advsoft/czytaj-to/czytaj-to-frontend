/**
 * Created by johniak on 1/7/17.
 */
import React, {Component, PropTypes} from 'react';
import UniversalContent from './UniversalContent';

const styles = {
  container: {
    paddingLeft: 10,
    borderLeft: '3px solid #bbb',
    paddingBottom: 10
  }
};

class Comment extends Component {

  render() {
    const {comment} = this.props;
    return (<div style={styles.container}><UniversalContent {...this.props} content={comment} type="comment"/></div>);
  }
}
Comment.propTypes = {
  comment: PropTypes.object.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

export default Comment;
