/**
 * Created by johniak on 1/14/17.
 */
import {store} from '../store/configureStore';

export class FetchInterceptor {
  constructor() {
    this.methods = [
      'OPTIONS',
      'GET',
      'HEAD',
      'POST',
      'PUT',
      'DELETE',
      'TRACE',
      'CONNECT',
      'PROPFIND',
      'PROPPATCH',
      'MKCOL',
      'COPY',
      'MOVE',
      'LOCK',
      'UNLOCK',
      'VERSION-CONTROL',
      'REPORT',
      'CHECKOUT',
      'CHECKIN',
      'UNCHECKOUT',
      'MKWORKSPACE',
      'UPDATE',
      'LABEL',
      'MERGE',
      'BASELINE-CONTROL',
      'MKACTIVITY',
      'ORDERPATCH',
      'ACL',
      'PATCH',
      'SEARCH'
    ];
  }

  getHeaders() {

  }

}

class Oauth2Interceptor extends FetchInterceptor {
  getHeaders() {
    const state = store.getState();
    if (!state.auth.session) {
      return {};
    }
    return {Authorization: `${state.auth.session.token_type} ${state.auth.session.access_token}`};
  }
}

export const interceptors = [Oauth2Interceptor];

export default function customFetch(url, options = {}) {
  console.log('fetchuje');
  let {headers} = options;
  if (!headers) {
    headers = {};
  }
  for (const Interceptor of interceptors) {
    const interceptorObj = new Interceptor();
    headers = Object.assign(headers, interceptorObj.getHeaders());
  }
  options.headers = headers;
  return fetch(url, options);
}
