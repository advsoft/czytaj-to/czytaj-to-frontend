/**
 * Created by johniak on 1/23/17.
 */

import fetch from './fetch';
import {API_URL} from "../constants/Settings";

export async function getPosts(offset, limit, orderBy, dateFrom, dateTo, tags, author) {
  const params =
    [`offset=${offset}`,
      `limit=${limit}`,
      `order_by=${orderBy}`];
  if (dateFrom) {
    params.push(`date_from=${dateFrom}`);
  }
  if (dateTo) {
    params.push(`date_to=${dateTo}`);
  }
  if (tags.length > 0) {
    const tagsParam = tags.join(',');
    params.push(`tags=${tagsParam}`);
  }
  if (author) {
    params.push(`author=${author}`);
  }
  const querystring = `?${params.join('&')}`;
  console.log(querystring, tags);
  const response = await fetch(`${API_URL}/microblog/posts/${querystring}`);
  const paginatedPosts = await response.json();
  return paginatedPosts;
}

export async function toggleLike(type, id, isLiked) {
  const config = {
    method: isLiked ? 'DELETE' : 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
  };
  const response = await fetch(`${API_URL}/microblog/${type}/${id}/like/`, config);
  console.log(Math.floor(response.status / 100));
  if (Math.floor(response.status / 100) !== 2) {
    const json = await response.json();
    throw json;
  }
  return true;
}
