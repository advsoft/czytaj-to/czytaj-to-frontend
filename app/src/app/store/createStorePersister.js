/**
 * Created by johniak on 1/9/17.
 */

function fillObject(path, value, obj) {
  path.split('.').reduce(
    (o, i, index, array) => {
      if (index + 1 >= array.length) {
        o[i] = value;
        return o[i];
      }
      if (o[i] === undefined) {
        o[i] = {};
      }
      return o[i];
    }, obj);
  return obj;
}

function pathToObject(path, obj) {
  return path.split('.').reduce((o, i) => o[i], obj);
}

function handleStoreChange(store, persistNames) {
  return function () {
    const state = store.getState();
    for (const persistName of persistNames) {
      const value = pathToObject(persistName, state);
      console.log(persistName, value);
      if (value === undefined) {
        localStorage.removeItem(persistName);
      } else {
        localStorage.setItem(persistName, JSON.stringify(value));
      }
    }
  };
}

export default function (persistNames = []) {
  const persistantInitial = {};
  for (const persistName of persistNames) {
    const json = localStorage.getItem(persistName);
    const object = JSON.parse(json);
    fillObject(persistName, object, persistantInitial);
  }
  return function (createStore) {
    return function (rootReducer, initialState = {}) {
      const store = createStore(rootReducer, Object.assign(initialState, persistantInitial));
      store.subscribe(handleStoreChange(store, persistNames));
      return store;
    };
  };
}
