import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers/index';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import createStorePersister from './createStorePersister';

const logger = createLogger();

export let store;

export default function configureStore(initialState) {
  const createStoreWithMiddleware = applyMiddleware(thunk, logger)(createStore);
  store = createStorePersister(['auth.session', 'auth.isAuthenticated'])(createStoreWithMiddleware)(rootReducer, initialState);
  console.log(store);
  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = rootReducer;
      store.replaceReducer(nextReducer);
    });
  }
  return store;
}
