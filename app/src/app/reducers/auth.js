import * as types from '../constants/ActionTypes';
import genericReducer from './genericReducer';

const initialState = {
  isAuthenticated: false
};

const allowedActions =
  [
    types.AUTH_SIGN_IN_SUCCESS,
    types.AUTH_SIGN_IN_REQUEST,
    types.AUTH_SIGN_IN_ERROR,
    types.AUTH_SIGN_OUT
  ];

export default genericReducer(initialState, allowedActions);
