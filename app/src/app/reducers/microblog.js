import * as types from '../constants/ActionTypes';
import genericReducer from './genericReducer';

const initialState = {
  posts: [],
  params: {}
};

const allowedActions =
  [
    types.MICROBLOG_GET_POSTS_REQUEST,
    types.MICROBLOG_GET_NEWEST_POSTS_ERROR,
    types.MICROBLOG_GET_FULL_POST_REQUEST,
    types.MICROBLOG_CREATE_COMMENT_REQUEST,
    types.MICROBLOG_CREATE_POST_REQUEST
  ];

function createCommentSuccess(state, {comment, parent}) {
  if (!parent.comments) {
    parent.comments = [];
  }
  parent.comments.push(comment);
  return Object.assign({}, state);
}

function getFullPostSuccess(state, post) {
  post.isFull = true;
  for (let i = 0; i < state.posts.length; i++) {
    const id = state.posts[i].id;
    if (id === post.id) {
      state.posts[i] = post;
    }
  }
  return Object.assign({}, state);
}

function createPostSuccess(state, post) {
  state.posts.splice(0, 0, post);
  return Object.assign({}, state);
}

function appendPosts(state, action) {
  const newState = Object.assign({}, state);
  if (action.params.offset === 0) {
    newState.posts = action.paginatedPosts.results;
  } else {
    newState.posts = [...newState.posts, ...action.paginatedPosts.results];
  }
  newState.params = action.params;
  newState.params.count = action.paginatedPosts.count;
  return newState;
}
function createLike(state, action) {
  const newState = Object.assign({}, state);
  // eslint-disable-next-line camelcase
  action.like.content.is_liked = true;
  // eslint-disable-next-line camelcase
  action.like.content.likes_count++;
  return newState;
}
function deleteLike(state, action) {
  const newState = Object.assign({}, state);
  // eslint-disable-next-line camelcase
  action.like.content.is_liked = false;
  // eslint-disable-next-line camelcase
  action.like.content.likes_count--;
  return newState;
}

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case types.MICROBLOG_GET_POSTS_SUCCESS:
      return appendPosts(state, action);
    case types.MICROBLOG_CREATE_COMMENT_SUCCESS:
      return createCommentSuccess(state, action.comment);
    case types.MICROBLOG_GET_FULL_POST_SUCCESS:
      return getFullPostSuccess(state, action.post);
    case types.MICROBLOG_CREATE_POST_SUCCESS:
      return createPostSuccess(state, action.post);
    case types.MICROBLOG_LIKE_CREATED:
      return createLike(state, action);
    case types.MICROBLOG_LIKE_DELETED:
      return deleteLike(state, action);
    default:
      return genericReducer(initialState, allowedActions)(state, action);
  }
}
