/**
 * Created by Johniak on 8/9/2016.
 */

/**
 * Creates reducer which copy whole action payload (action object without state) into
 * State is still immutable.
 * @param initialState
 * @param allowedActions
 * @returns {copyReducer}
 */
export default (initialState, allowedActions) => {
  return (state = initialState, action = {}) => {
    if (allowedActions.indexOf(action.type) < 0) {
      return state;
    }
    const newState = Object.assign({}, state);
    const actionKeys = Object.keys(action).filter(item => item !== 'type');
    for (const key of actionKeys) {
      newState[key] = action[key];
    }
    return newState;
  };
};
