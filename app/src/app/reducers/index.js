import {combineReducers} from 'redux';
import microblog from './microblog';
import auth from './auth';
const rootReducer = combineReducers({
  microblog,
  auth
});

export default rootReducer;
