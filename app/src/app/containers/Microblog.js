/**
 * Created by johniak on 1/6/17.
 */
import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PostList from '../components/PostsList';
import AddPost from '../components/AddPost';
import Header from './Header';
import * as MicroblogActions from '../actions/microblog';

const styles = {
  container: {
    background: '#111111',
    color: '#fff',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: '40px'
  },
  posts: {
    width: '100%',
    maxWidth: 1000,
    marginTop: 20
  },
  border: {
    marginLeft: 10,
    marginRight: 10
  }
};

class Microblog extends Component {

  componentWillMount() {
    this.isAuthenticated = false;
  }

  render() {
    const {auth} = this.props;
    return (
      <div>
        <Header {...this.props}/>
        <div style={styles.container}>
          <div style={styles.posts}>
            <div style={styles.border}>

              {auth.isAuthenticated ?
                <AddPost {...this.props}/> : null
              }
              <PostList {...this.props}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Microblog.propTypes = {
  microblog: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    microblog: state.microblog,
    auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(MicroblogActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Microblog);

