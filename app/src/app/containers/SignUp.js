/**
 * Created by johniak on 1/6/17.
 */
import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import SignUpComponent from '../components/SignUpComponent';
import * as AuthActions from '../actions/auth';
import Header from './Header';
const styles = {
  container: {
    background: '#111111',
    color: '#fff',
    width: '100%',
    height: 'calc(100vh - 40px)',
    paddingTop: 40,
    display: 'flex'
  }
};

class Microblog extends Component {
  componentWillMount() {
  }

  render() {
    const {actions} = this.props;
    return (
      <div>
        <Header {...this.props}/>
        <div style={styles.container}>
          <SignUpComponent {...this.props} signIn={actions.signIn}/>
        </div>
      </div>
    );
  }
}

Microblog.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(AuthActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Microblog);

