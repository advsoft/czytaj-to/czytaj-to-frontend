/**
 * Created by johniak on 1/7/17.
 */
import * as types from '../constants/ActionTypes';
import fetch from '../network/fetch';
import {API_URL} from "../constants/Settings";
import * as api from '../network/api';

function getPostsRequest(params) {
  return {type: types.MICROBLOG_GET_POSTS_REQUEST, params};
}

function getPostsSuccess(paginatedPosts, params) {
  return {type: types.MICROBLOG_GET_POSTS_SUCCESS, paginatedPosts, params};
}

// function getNewestPostsError(error) {
//   return {type: types.MICROBLOG_GET_NEWEST_POSTS_ERROR, error};
// }

function getFullPostRequest(requestedPost) {
  return {type: types.MICROBLOG_GET_FULL_POST_REQUEST, requestedPost};
}

function getFullPostSuccess(post) {
  return {type: types.MICROBLOG_GET_FULL_POST_SUCCESS, post};
}

// function getFullPostError(error) {
//   return {type: types.MICROBLOG_GET_FULL_POST_ERROR, error};
// }

function createCommentRequest(type, parent, content) {
  return {
    type: types.MICROBLOG_CREATE_COMMENT_REQUEST, comment: {
      type, parent, content
    }
  };
}

function createCommentSuccess(comment, type, parent) {
  return {
    type: types.MICROBLOG_CREATE_COMMENT_SUCCESS,
    comment: {comment, type, parent}
  };
}

// function getNewestPostsError(error) {
//   return {type: types.MICROBLOG_CREATE_COMMENT_ERROR, error};
// }

function createPosttRequest(content) {
  return {
    type: types.MICROBLOG_CREATE_POST_REQUEST, post: {
      content
    }
  };
}

function createPostSuccess(post) {
  return {
    type: types.MICROBLOG_CREATE_POST_SUCCESS,
    post
  };
}

function likeRequest(type, id, isLiked) {
  return {
    type: types.MICROBLOG_LIKE_REQUEST, likeRequest: {
      type, id, isLiked
    }
  };
}

function likeCreated(type, id, content) {
  return {
    type: types.MICROBLOG_LIKE_CREATED, like: {
      type, id, content
    }
  };
}

function likeDeleted(type, id, content) {
  return {
    type: types.MICROBLOG_LIKE_DELETED, like: {
      type, id, content
    }
  };
}

// function createPostError(error) {
//   return {type: types.MICROBLOG_CREATE_POST_ERROR, error};
// }
export function getPosts(offset = 0, limit = 2, {orderBy, dateFrom, dateTo, tags, author} = {
  orderBy: '-date',
  dateFrom: null,
  dateTo: null,
  tags: [],
  author: null
}) {
  if (!orderBy) {
    orderBy = '-date';
  }
  if (!tags) {
    tags = [];
  }
  return async function (dispatch) {
    console.log('tags', tags);
    dispatch(getPostsRequest({offset, limit, orderBy, dateFrom, dateTo, tags, author}));
    const paginatedPosts = await api.getPosts(offset, limit, orderBy, dateFrom, dateTo, tags, author);
    dispatch(getPostsSuccess(paginatedPosts, {offset, limit, orderBy, dateFrom, dateTo, tags, author}));
  };
}

async function getPostRequest(id) {
  const response = await fetch(`${API_URL}/microblog/post/${id}/`);
  return await response.json();
}

export function getFullPost(post) {
  return async function (dispatch) {
    dispatch(getFullPostRequest(post));
    const fullPost = await getPostRequest(post.id);
    dispatch(getFullPostSuccess(fullPost));
  };
}

export function createPost(content, medias, articleUrl) {
  return async function (dispatch) {
    dispatch(createPosttRequest(content));
    const payload = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      // eslint-disable-next-line camelcase
      body: JSON.stringify({content, media_set: medias, article_url: articleUrl})
    };
    const response = await fetch(`${API_URL}/microblog/post/`, payload);
    const post = await response.json();
    dispatch(createPostSuccess(post));
  };
}

export function createComment(type, parent, content) {
  return async function (dispatch) {
    dispatch(createCommentRequest(type, parent, content));
    const payload = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({content})
    };
    const response = await fetch(`${API_URL}/microblog/${type}/${parent.id}/comment/`, payload);
    const comment = await response.json();
    dispatch(createCommentSuccess(comment, type, parent));
  };
}

export function toggleLike(type, id, isLiked, content) {
  return async function (dispatch) {
    dispatch(likeRequest(type, id, isLiked));
    const like = await api.toggleLike(type, id, isLiked);
    console.log('like', like);
    if (isLiked) {
      dispatch(likeDeleted(type, id, content));
    } else {
      dispatch(likeCreated(type, id, content));
    }
  };
}
