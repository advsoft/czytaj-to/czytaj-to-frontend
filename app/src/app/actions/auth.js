/**
 * Created by johniak on 1/7/17.
 */
import * as types from '../constants/ActionTypes';
import {API_URL} from "../constants/Settings";

function signInRequest() {
  return {type: types.AUTH_SIGN_IN_REQUEST, error: undefined};
}

function signInSuccess(session) {
  return {type: types.AUTH_SIGN_IN_SUCCESS, session, error: undefined, isAuthenticated: Boolean(session)};
}

function signInError(error) {
  return {type: types.AUTH_SIGN_IN_ERROR, error, session: null};
}

export function signOut() {
  return {type: types.AUTH_SIGN_OUT, session: undefined, error: undefined, isAuthenticated: undefined};
}

export function signIn(username, password) {
  return async function (dispatch) {
    dispatch(signInRequest());
    const payload = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({username, password})
    };
    const response = await fetch(`${API_URL}/oauth2/token/?client_id=wfrbd4HGASyyRc1Qbmhk5zK97otNUaTjlqQbRFHg&grant_type=password`, payload);
    if (response.status !== 200) {
      const error = await response.json();
      dispatch(signInError(error));
      return;
    }
    const session = await response.json();
    dispatch(signInSuccess(session));
  };
}
