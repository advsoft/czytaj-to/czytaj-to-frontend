import 'babel-polyfill';
import 'whatwg-fetch';

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import configureStore from './app/store/configureStore';
import {Router, Route, browserHistory} from 'react-router';

import Microblog from "./app/containers/Microblog";
// import Tag from "./app/containers/Tag";
import SignIn from "./app/containers/SignIn";

import './index.scss';
import {StyleRoot} from 'radium';
const store = configureStore();

render(
  <StyleRoot>
    <Provider store={store}>
      <Router history={browserHistory}>
        <Route path="/signin/" component={SignIn}/>
        <Route name="main" path="/(:type/)" component={Microblog}/>
        <Route name="tag" path="/tag/:tag(/:type/)" component={Microblog}/>
        <Route name="tag" path="/user/:user(/:type/)" component={Microblog}/>
      </Router>
    </Provider>
  </StyleRoot>,
  document.getElementById('root')
);
